<?php
/**
 * @var $connection
 */
require 'connect-to-database.php';

$idToFind = $_GET["id"];

$query = "SELECT id, name, username, password, age, role, profile, email, webpage FROM user where id = ?";

$statement = $connection->prepare($query);
if (!$statement->bind_param("i", $idToFind)) {
    echo "Operation failed. Check again the input values.";
}
$statement->execute();
$statement->bind_result($id, $name, $username, $password, $age, $role, $profile, $email, $webpage);
$statement->fetch();

$user = new StdClass();
$user->id = $id;
$user->name = $name;
$user->username = $username;
$user->password = $password;
$user->age = $age;
$user->role = $role;
$user->profile = $profile;
$user->email = $email;
$user->webpage = $webpage;

echo json_encode($user);

$statement->close();
$connection->close();
