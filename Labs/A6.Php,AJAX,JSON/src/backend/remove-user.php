<?php
/**
 * @var $connection
 */
require 'connect-to-database.php';

$id = $_POST["id"];

$query = "delete from user where id = ?";

$statement = $connection->prepare($query);
if (!$statement->bind_param("i", $id)) {
    echo "Operation failed. Check again the input values.";
}

if ($statement->execute()) {
    echo "User was removed successfully";
}

$statement->close();
$connection->close();
