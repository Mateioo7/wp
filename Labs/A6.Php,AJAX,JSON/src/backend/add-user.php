<?php
/**
 * @var $connection
 */
require 'connect-to-database.php';

$nameToAdd = $_POST["name"];
$usernameToAdd = $_POST["username"];
$passwordToAdd = $_POST["password"];
$ageToAdd = $_POST["age"];
$roleToAdd = $_POST["role"];
$profileToAdd = $_POST["profile"];
$emailToAdd = $_POST["email"];
$webpageToAdd = $_POST["webpage"];

$query = "insert into user (name, username, password, age, role, profile, email, webpage)
        values (?, ?, ?, ?, ?, ?, ?, ?)";

$statement = $connection->prepare($query);
if (!$statement->bind_param("sssissss", $nameToAdd, $usernameToAdd,
    $passwordToAdd, $ageToAdd, $roleToAdd, $profileToAdd, $emailToAdd, $webpageToAdd)) {
    echo "Operation failed. Check again the input values.";
}

if ($statement->execute()) {
    echo "User was added successfully";
}

$statement->close();
$connection->close();
