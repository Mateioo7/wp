<?php

header('Access-Control-Allow-Origin: *');

$host = "127.0.0.1";
$username = "root";
$password = "root";
$dbName = "web-programming";

// Create connection
$connection = new mysqli($host, $username, $password, $dbName);

// Check connection
if ($connection->connect_error) {
    die("Connection failed: " . $connection->connect_error);
}

// we can now safely use $connection in the other php files
