<?php
/**
 * @var $connection
 */
require 'connect-to-database.php';

$nameToFind = $_GET["name"];
$limit = $_GET["limit"];

if ($limit <= 0) {
    $limit = 10;
}

$query = "SELECT id, name, username, password, age, role, profile, email, webpage FROM user where name like ? limit ?";

$statement = $connection->prepare($query);
$nameParam = '%' . $nameToFind . '%';
if (!$statement->bind_param("si", $nameParam, $limit)) {
    echo "Operation failed. Check again the input values.";
}
$statement->execute();
$statement->bind_result($id, $name, $username, $password, $age, $nameToFind, $profile, $email, $webpage);

$users = array();

while ($statement->fetch()) {
    $user = new StdClass();

    $user->id = $id;
    $user->name = $name;
    $user->username = $username;
    $user->password = $password;
    $user->age = $age;
    $user->role = $nameToFind;
    $user->profile = $profile;
    $user->email = $email;
    $user->webpage = $webpage;

    array_push($users, $user);
}

echo json_encode($users);

$statement->close();
$connection->close();
