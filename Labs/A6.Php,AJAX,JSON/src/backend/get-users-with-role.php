<?php
/**
 * @var $connection
 */
require 'connect-to-database.php';

$role = $_GET["role"];
$limit = $_GET["limit"];

if ($limit <= 0) {
    $limit = 10;
}

$query = "SELECT id, name, username, password, age, role, profile, email, webpage FROM user where role = ? limit ?";

$statement = $connection->prepare($query);
if (!$statement->bind_param("si", $role, $limit)) {
    echo "Operation failed. Check again the input values.";
}
$statement->execute();
$statement->bind_result($id, $name, $username, $password, $age, $role, $profile, $email, $webpage);

$users = array();

while ($statement->fetch()) {
    $user = new StdClass();

    $user->id = $id;
    $user->name = $name;
    $user->username = $username;
    $user->password = $password;
    $user->age = $age;
    $user->role = $role;
    $user->profile = $profile;
    $user->email = $email;
    $user->webpage = $webpage;

    array_push($users, $user);
}

echo json_encode($users);

$statement->close();
$connection->close();
