<?php
/**
 * @var $connection
 */
require 'connect-to-database.php';

$id = $_POST["id"];
$newName = $_POST["name"];
$newUsername = $_POST["username"];
$newPassword = $_POST["password"];
$newAge = $_POST["age"];
$newRole = $_POST["role"];
$newProfile = $_POST["profile"];
$newEmail = $_POST["email"];
$newWebpage = $_POST["webpage"];

$query = "update user set 
            name = ?,
            username = ?,
            password = ?,
            age = ?,
            role = ?,
            profile = ?,
            email = ?,
            webpage = ?
        where id = ?";

$statement = $connection->prepare($query);
if (!$statement->bind_param("sssissssi", $newName, $newUsername,
    $newPassword, $newAge, $newRole, $newProfile, $newEmail, $newWebpage, $id)) {
    echo "Operation failed. Check again the input values.";
}

if ($statement->execute()) {
    echo "User was updated successfully";
}

$statement->close();
$connection->close();
