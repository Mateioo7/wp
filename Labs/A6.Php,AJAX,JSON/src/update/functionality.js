$(document).ready(function () {
    let parameters = {}
    parameters['id'] = getUrlParameterValue('id');

    $.getJSON(
        "http://localhost/projects/A6.Php,AJAX,JSON/src/backend/get-user-with-id.php",
        parameters,
        autocompleteInputs
    );

    function autocompleteInputs(data) {
        $("#id-input").val(data.id);
        $("#name-input").val(data.name);
        $("#username-input").val(data.username);
        $("#password-input").val(data.password);
        $("#age-input").val(data.age);
        $("#role-input").val(data.role);
        $("#profile-input").val(data.profile);
        $("#email-input").val(data.email);
        $("#webpage-input").val(data.webpage);
    }

    function getUrlParameterValue(paramName) {
        let searchParams = new URLSearchParams(window.location.search);
        return searchParams.get(paramName)
    }

    $("#submit").click(function () {
        $.ajax({
            type: "POST",
            url: "http://localhost/projects/A6.Php,AJAX,JSON/src/backend/update-user.php",
            data: {
                id: $("#id-input").val(),
                name: $("#name-input").val(),
                username: $("#username-input").val(),
                password: $("#password-input").val(),
                age: $("#age-input").val(),
                role: $("#role-input").val(),
                profile: $("#profile-input").val(),
                email: $("#email-input").val(),
                webpage: $("#webpage-input").val()
            },
            success: showStatus
        });

        function showStatus(data) {
            alert(data);
        }
    });
});
