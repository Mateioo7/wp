$(document).ready(function() {
    $("#desktop-1").click(function () {
        hideDesktopToTheRight(this);
        showDesktop($("#desktop-2"));
    });

    $("#desktop-2").click(function (event) {
        if (clickedOnRightHalf(event)) {
            hideDesktopToTheLeft(this);
            showDesktop($("#desktop-1"));
        }
        else {
            hideDesktopToTheRight(this);
            showDesktop($("#desktop-3"));
        }
    });

    $("#desktop-3").click(function (event) {
        if (clickedOnRightHalf(event)) {
            hideDesktopToTheLeft(this);
            showDesktop($("#desktop-2"));
        }
        else {
            hideDesktopToTheRight(this);
            showDesktop($("#desktop-4"));
        }
    });

    $("#desktop-4").click(function () {
        hideDesktopToTheLeft(this);
        showDesktop($("#desktop-3"));
    });

    function hideDesktopToTheLeft(desktopSelector) {
        $(desktopSelector).animate({ left: "-100%", width: "0" }, "slow");
    }

    function hideDesktopToTheRight(desktopSelector) {
        $(desktopSelector).animate({ left: "100%"}, "slow");
    }

    function showDesktop(desktopSelector) {
        $(desktopSelector).animate({ left: "0%", width: "100%" }, "slow");
    }

    // event.pageX refers to the x position of the event (click)
    // this is fine as the desktops are placed in the middle of the page
    function clickedOnRightHalf(event) {
        return event.pageX >= $(document).width() / 2;
    }
});