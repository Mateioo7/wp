export interface Website {
  id: number;
  url: string;
}
