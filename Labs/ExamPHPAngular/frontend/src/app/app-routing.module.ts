import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {Entity1DashboardComponent} from './entity1-dashboard/entity1-dashboard.component';
import {UpdateEntityComponent} from './update-entity/update-entity.component';
import {CreateEntityComponent} from './create-entity/create-entity.component';
import {LoginComponent} from './login/login.component';
import {LogoutComponent} from './logout/logout.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'websites', component: Entity1DashboardComponent },
  { path: 'update/:websiteId', component: UpdateEntityComponent },
  { path: 'create-website', component: CreateEntityComponent },
  { path: 'logout', component: LogoutComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
