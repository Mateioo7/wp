import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { Entity1DashboardComponent } from './entity1-dashboard/entity1-dashboard.component';
import { AppRoutingModule } from './app-routing.module';
import {HttpClientModule} from '@angular/common/http';
import { UpdateEntityComponent } from './update-entity/update-entity.component';
import { CreateEntityComponent } from './create-entity/create-entity.component';
import {CookieService} from 'ngx-cookie-service';
import { LoginComponent } from './login/login.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { LogoutComponent } from './logout/logout.component';

@NgModule({
  declarations: [
    AppComponent,
    Entity1DashboardComponent,
    UpdateEntityComponent,
    CreateEntityComponent,
    LoginComponent,
    LogoutComponent,
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
    ],
  providers: [CookieService],
  bootstrap: [AppComponent]
})
export class AppModule { }
