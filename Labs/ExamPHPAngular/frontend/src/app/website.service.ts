import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Website} from './website';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WebsiteService {
  constructor(private http: HttpClient) { }
  private backendUrl = 'http://localhost/projects/ExamPHPAngular/backend/';

  getAll(): Observable<Website[]> {
    return this.http.get<Website[]>(this.backendUrl + 'get-entities.php');
  }

  getWithId(id): Observable<Website> {
    const params = new HttpParams().set('id', id);
    return this.http.get<Website>(this.backendUrl + 'get-entity-by-id.php', {params});
  }

  update(id, url): void {
    const params = new HttpParams()
      .set('id', id)
      .set('url', url);

    this.http.post<Website>(this.backendUrl + 'update-entity.php', params).subscribe();
  }

  create(url): void {
    const params = new HttpParams().set('url', url);
    this.http.post<Website>(this.backendUrl + 'create-entity.php', params).subscribe();
  }

  delete(id): void {
    const params = new HttpParams().set('id', id);
    this.http.post<Website>(this.backendUrl + 'delete-entity.php', params).subscribe();
  }

  // todo: use something similar when checking if login fields are correct
  test(url): Observable<any> {
    const params = new HttpParams().set('url', url);
    return this.http.get(this.backendUrl + 'test.php', {
      params,
      responseType: 'text'
    });
  }
}
