import { Component, OnInit } from '@angular/core';
import {WebsiteService} from '../website.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Website} from '../website';
import {CookieService} from 'ngx-cookie-service';

@Component({
  selector: 'app-update-entity',
  templateUrl: './update-entity.component.html',
  styleUrls: ['./update-entity.component.css']
})
export class UpdateEntityComponent implements OnInit {
  constructor(private service: WebsiteService, private route: ActivatedRoute,
              private router: Router, private cookieService: CookieService) { }
  website: Website;
  username: string;

  ngOnInit(): void {
    this.username = this.cookieService.get('username');
    const routeParams = this.route.snapshot.paramMap;
    const idFromRoute = Number(routeParams.get('websiteId'));
    this.service.getWithId(idFromRoute).subscribe(website => this.website = website);
  }

  updateEntity(): void {
    const routeParams = this.route.snapshot.paramMap;
    const idFromRoute = Number(routeParams.get('websiteId'));

    const url = (document.getElementById('url-input') as HTMLInputElement).value;

    this.service.update(idFromRoute, url);
    this.router.navigate(['websites']).then(() => {});
  }
}
