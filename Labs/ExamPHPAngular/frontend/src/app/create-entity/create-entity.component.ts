import {Component, OnInit} from '@angular/core';
import {WebsiteService} from '../website.service';
import {Router} from '@angular/router';
import {CookieService} from 'ngx-cookie-service';

@Component({
  selector: 'app-add-entity',
  templateUrl: './create-entity.component.html',
  styleUrls: ['./create-entity.component.css']
})
export class CreateEntityComponent implements OnInit {
  constructor(private service: WebsiteService, private cookieService: CookieService,
              private router: Router) {
  }

  ngOnInit(): void {
    console.log(this.cookieService.get('username'));
    // if (this.cookieService.get('username'))
  }

  createEntity(): void {
    // db date: YYYY-MM-DD
    // db datetime: YYYY-MM-DD hh:mm:ss
    // const currentMySQLDate = new Date().toISOString().slice(0, 10);
    // const currentMySQLDateTime = new Date().toISOString().slice(0, 10) + ' ' + new Date().toLocaleTimeString('en-GB');

    const url = (document.getElementById('url-input') as HTMLInputElement).value;
    this.service.create(url);
    this.router.navigate(['websites']).then(() => {});
  }
}
