import { Component, OnInit } from '@angular/core';
import {CookieService} from 'ngx-cookie-service';
import {Router} from '@angular/router';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  constructor(private cookieService: CookieService, private router: Router,
              private fb: FormBuilder) { }
  // username: string;
  loginForm = this.fb.group({
    username: '' // ['', Validators.required ],
  });

  ngOnInit(): void {
  }

  login(): void {
    this.cookieService.set('username', this.loginForm.get('username').value);
    this.router.navigate(['websites']).then(() => {});
  }
}
