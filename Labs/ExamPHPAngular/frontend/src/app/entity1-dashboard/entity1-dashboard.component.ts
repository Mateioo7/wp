import { Component, OnInit } from '@angular/core';
import {Website} from '../website';
import { WebsiteService } from '../website.service';
import {CookieService} from 'ngx-cookie-service';

@Component({
  selector: 'app-entity1-dashboard',
  templateUrl: './entity1-dashboard.component.html',
  styleUrls: ['./entity1-dashboard.component.css']
})
export class Entity1DashboardComponent implements OnInit {
  constructor(private service: WebsiteService, private cookieService: CookieService) { }

  test: string;
  websites: Website[];
  username: string;

  ngOnInit(): void {
    this.service.test('abcde').subscribe(output => { this.test = output; });

    this.username = this.cookieService.get('username');
    this.service.getAll().subscribe(websites => this.websites = websites);
  }

  delete(id): void {
    if (confirm('Confirm deletion?')) {
      this.service.delete(id);
      location.reload();
    }
  }
}
