<?php
/**
 * @var $connection
 */
require 'database-connection.php';

header("Access-Control-Allow-Headers: *");
header("Access-Control-Allow-Origin: *");

$id = $_POST["id"];
$url = $_POST["url"];

$query = "update websites set url = ? where id = ?";

$statement = $connection->prepare($query);
$statement->bind_param("si", $url, $id);
$statement->execute();

$statement->close();
$connection->close();
