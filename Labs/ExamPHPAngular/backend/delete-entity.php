<?php
/**
 * @var $connection
 */
require 'database-connection.php';

$id = $_POST["id"];

$query = "delete from websites where id = ?";

$statement = $connection->prepare($query);
$statement->bind_param("i", $id);
$statement->execute();

$statement->close();
$connection->close();
