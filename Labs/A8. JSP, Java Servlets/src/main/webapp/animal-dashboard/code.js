function deleteAnimal(id) {
    if (confirm("Are you sure you wish to delete the animal?"))
        $.ajax({
            url: `animal-delete`,
            method: 'post',
            data: {
                id: id
            },
            success: function () {
                location.reload()
            }
        })
}

function updateAnimal(id) {
    $.ajax({
        url: `animal-update`,
        method: 'get',
        data: {
            id: id
        },
        success: function () {
            // location.reload()
        }
    })
}
