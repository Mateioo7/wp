<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Login page</title>

    <script src="plugins/jquery-3.6.0.js"></script>
    <script src="plugins/jquery-validation/dist/jquery.validate.js"></script>
    <script src="login/login.js"></script>

    <link rel="stylesheet" href="${pageContext.request.contextPath}/login/login.css">
</head>
<body>
<div style="text-align: center">
    <h1>Login</h1>
    <form id="loginForm" action="login" method="post">
        <label>Username:</label>
        <label>
            <input name="username" />
        </label>
        <br><br>
        <label>Password:</label>
        <label>
            <input type="password" name="password" />
        </label>
        <div class="error">${message}</div>
        <button type="submit">Login</button>
    </form>
</div>
</body>
</html>
