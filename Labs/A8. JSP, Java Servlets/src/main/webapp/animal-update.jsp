<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Update Animal</title>
</head>
<body>

<form method="post" action="animal-update">
    <input type="hidden" id="id" name="id" value="${animal.id}">

    <label for="species">Species:</label>
    <input type="text" id="species" name="species" value="${animal.species}"><br>

    <label for="age">Age:</label>
    <input type="text" id="age" name="age" value="${animal.age}"><br>

    <label for="name">Name:</label>
    <input type="text" id="name" name="name" value="${animal.name}"><br>

    <input type="submit" value="Update">
</form>

</body>
</html>
