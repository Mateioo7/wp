package com.example.A8_JSP_JavaServlets.lab;

import com.example.A8_JSP_JavaServlets.domain.Image;
import com.example.A8_JSP_JavaServlets.services.ImageService;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.List;

@WebServlet(name = "uploadImageServlet", value = "/upload-image")
@MultipartConfig // needed for request.getPart()
public class UploadImageServlet extends HttpServlet {
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getSession().getAttribute("username") == null) {
            try {
                response.sendRedirect("login.jsp");
                return;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        Part filePart = request.getPart("file"); // Retrieves <input type="file" name="file">
        InputStream fileContent = filePart.getInputStream();

        try {
            String author = (String) request.getSession().getAttribute("username");

            ImageService imageService = new ImageService();
            imageService.uploadImage(fileContent, author);

            List<Image> imageList = imageService.getAllImages();
            request.setAttribute("imageList", imageList);

            response.sendRedirect("dashboard");
        } catch (SQLException | ClassNotFoundException | IOException throwable) {
            throwable.printStackTrace();
        }
    }
}
