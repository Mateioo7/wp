package com.example.A8_JSP_JavaServlets.lab;

import com.example.A8_JSP_JavaServlets.domain.Image;
import com.example.A8_JSP_JavaServlets.services.ImageService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@WebServlet(name = "bestImagesServlet", value = "/best-images")
public class BestImagesServlet extends HttpServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        if (session.getAttribute("username") == null) {
            try {
                response.sendRedirect("login.jsp");
                return;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            String topValue = request.getParameter("topValue");

            ImageService imageService = new ImageService();
            List<Image> imageList = imageService.getBestImages(topValue);
            request.setAttribute("imageList", imageList);

            String username = (String) session.getAttribute("username");
            request.setAttribute("username", username);

            request.getRequestDispatcher("dashboard.jsp").forward(request, response);
        } catch (SQLException | ClassNotFoundException | ServletException | IOException throwable) {
            throwable.printStackTrace();
        }
    }
}
