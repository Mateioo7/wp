package com.example.A8_JSP_JavaServlets.lab;

import com.example.A8_JSP_JavaServlets.domain.Image;
import com.example.A8_JSP_JavaServlets.services.ImageService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class DashboardServlet extends HttpServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse response) {
        if (request.getSession().getAttribute("username") == null) {
            try {
                response.sendRedirect("login.jsp");
                return;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            ImageService imageService = new ImageService();
            List<Image> imageList = imageService.getAllImages();
            request.setAttribute("imageList", imageList);

            String username = (String) request.getSession().getAttribute("username");
            request.setAttribute("username", username);

            request.getRequestDispatcher("dashboard.jsp").forward(request, response);
        } catch (SQLException | ClassNotFoundException | IOException | ServletException throwable) {
            throwable.printStackTrace();
        }
    }
}
