package com.example.A8_JSP_JavaServlets.services;

import com.example.A8_JSP_JavaServlets.domain.Image;

import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ImageService {
    private Connection connection;

    public ImageService() throws SQLException, ClassNotFoundException {
        this.initializeConnection();
    }

    private void initializeConnection() throws ClassNotFoundException, SQLException {
        String url = "jdbc:mysql://localhost:3306";
        String user = "root";
        String password = "root";

        Class.forName("com.mysql.cj.jdbc.Driver");
        this.connection = DriverManager.getConnection(url, user, password);
    }

    public List<Image> getAllImages() throws SQLException {
        String sql = "select * from `web-programming`.jsp_image";
        PreparedStatement statement = connection.prepareStatement(sql);
        ResultSet resultSet = statement.executeQuery();

        List<Image> imageList = new ArrayList<>();
        while (resultSet.next()) {
            Image image = new Image();
            image.setId(Integer.parseInt(resultSet.getString("id")));
            image.setAuthor(resultSet.getString("author"));
            image.setVotesNumber(Integer.parseInt(resultSet.getString("votesNumber")));
            image.setScore(Double.parseDouble(resultSet.getString("score")));
            imageList.add(image);
        }

        return imageList;
    }

    public void submitVote(String imageId, String voteValue) throws SQLException {
        String sql = "select * from `web-programming`.jsp_image where id = ?";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, imageId);

        ResultSet resultSet = statement.executeQuery();
        Image image = null;
        if (resultSet.next()) {
            image = new Image();
            image.setId(Integer.parseInt(resultSet.getString("id")));
            image.setVotesNumber(Integer.parseInt(resultSet.getString("votesNumber")));
            image.setScore(Double.parseDouble(resultSet.getString("score")));
        }

        if (image == null) {
            return;
        }

        int newVotesNumber = image.getVotesNumber() + 1;
        double newScore = image.getScore() + Integer.parseInt(voteValue);

        sql = "update `web-programming`.jsp_image set votesNumber = ?, score = ? where id = ?";

        statement = connection.prepareStatement(sql);
        statement.setString(1, String.valueOf(newVotesNumber));
        statement.setString(2, String.valueOf(newScore));
        statement.setString(3, imageId);

        statement.executeUpdate();
    }

    public void uploadImage(InputStream fileContent, String author) throws SQLException {
        String sql = "insert into `web-programming`.jsp_image(content, author, votesNumber, score)" +
                " values (?, ?, 0, 0)";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setBinaryStream(1, fileContent);
        statement.setString(2, author);

        statement.executeUpdate();
    }

    public byte[] getImageContent(String imageId) throws SQLException {
        String sql = "select * from `web-programming`.jsp_image where id = ?";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, imageId);

        ResultSet resultSet = statement.executeQuery();
        if (resultSet.next()) {
            return resultSet.getBytes("content");
        }

        return new byte[]{};
    }

    public List<Image> getBestImages(String topValue) throws SQLException {
        String sql = "select * from `web-programming`.jsp_image order by score desc limit ?";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(1, Integer.parseInt(topValue));
        ResultSet resultSet = statement.executeQuery();

        List<Image> imageList = new ArrayList<>();
        while (resultSet.next()) {
            Image image = new Image();
            image.setId(Integer.parseInt(resultSet.getString("id")));
            image.setAuthor(resultSet.getString("author"));
            image.setVotesNumber(Integer.parseInt(resultSet.getString("votesNumber")));
            image.setScore(Double.parseDouble(resultSet.getString("score")));
            imageList.add(image);
        }

        return imageList;
    }
}
