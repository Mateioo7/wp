package com.example.A8_JSP_JavaServlets.lab;

import com.example.A8_JSP_JavaServlets.domain.Image;
import com.example.A8_JSP_JavaServlets.services.ImageService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@WebServlet(name = "submitVoteServlet", value = "/submit-vote-servlet")
public class SubmitVoteServlet extends HttpServlet {
    public void doPost(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        if (session.getAttribute("username") == null) {
            try {
                response.sendRedirect("login.jsp");
                return;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        String imageId = request.getParameter("imageId");
        String voteValue = request.getParameter("voteValue");

        try {
            ImageService imageService = new ImageService();
            imageService.submitVote(imageId, voteValue);

            List<Image> imageList = imageService.getAllImages();
            request.setAttribute("imageList", imageList);
            request.getRequestDispatcher("dashboard.jsp").forward(request, response);
        } catch (SQLException | ClassNotFoundException | ServletException | IOException throwable) {
            throwable.printStackTrace();
        }


    }
}
