package com.example.A8_JSP_JavaServlets.practice;

import com.example.A8_JSP_JavaServlets.domain.Animal;
import com.example.A8_JSP_JavaServlets.services.AnimalRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "animalUpdateController", value = "/animal-update")
public class AnimalUpdateController extends HttpServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse response) {
        if (request.getSession().getAttribute("username") == null) {
            try {
                response.sendRedirect("login.jsp");
                return;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            AnimalRepository animalRepository = new AnimalRepository();
            String id = request.getParameter("id");
            Animal animal = animalRepository.getAnimal(id);
            request.setAttribute("animal", animal);

            String username = (String) request.getSession().getAttribute("username");
            request.setAttribute("username", username);

            request.getRequestDispatcher("animal-update.jsp").forward(request, response);
        } catch (SQLException | ClassNotFoundException | IOException | ServletException throwable) {
            throwable.printStackTrace();
        }
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) {
        if (request.getSession().getAttribute("username") == null) {
            try {
                response.sendRedirect("login.jsp");
                return;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            AnimalRepository animalRepository = new AnimalRepository();

            String id = request.getParameter("id");
            String species = request.getParameter("species");
            String age = request.getParameter("age");
            String name = request.getParameter("name");

            animalRepository.updateAnimal(id, species, age, name);

            response.sendRedirect("animal-dashboard");
        } catch (SQLException | ClassNotFoundException | IOException throwable) {
            throwable.printStackTrace();
        }
    }
}
