<?php
include ('connection.php');
session_start();
if (isset($_SESSION['id'])){
    $userId = $_SESSION['id'];
    $username = $_SESSION['username'];

}
else {
    header('Location: login.php');
    die();
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Welcome user</title>
    <script src="jquery-3.6.0.min.js"></script>
    <script src="main.js"></script>
    <link rel="stylesheet" type="text/css" href="main.css">
</head>
<body>
<h3>Welcome  <?php echo $username?>! </h3>

<br>
<div>
    <button type="button" onclick="showMostAuthorsDocument()">Show Most authored</button>
    <br>
</div>
<section id="allItems"> </section>
<br>

<button type="button" onclick="showMatches('<?php echo $username?>')">Show my work</button>
<br>
<section id="subscriptions"> </section>

<br><section id="documents"> </section><br>

<br>
<div>
    <label for="doc-name">Name</label><br><input name="docname" type="text" id="doc-name" placeholder="Doc name"> <br>
    <label for="content-input">COntents</label><br><input name="content" type="text" id="content-input" placeholder="Content"> <br>
    <button type="button" onclick="add()">Add new doc</button>
    <br><br>
    <section id="subscribe-message"> </section>
</div>

<br>
<form action="logout.php">
    <input type="submit" name="logout" value="Logout">
</form>
<br>

</body>
</html>
