<?php
include ('connection.php');
//include ('Asset.php');

try {
    //populate not used
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $con = OpenConnection();
        $json = json_decode(file_get_contents('php://input'), true);

        $uid = $json['userid'];

        header('HTTP/1.1 200 OK');

        CloseConnection($con);
        exit;
    }
    //add new Doc
    else if (isset($_GET['name']) and isset($_GET['contents']))
    {
        $name = $_GET['name'];
        $contents = $_GET['contents'];
        $con = OpenConnection();
        $query = "INSERT INTO documents(name, contents) VALUES('".$name."', '".$contents."');";
        $con->query($query);
        echo "Add success";
        header('HTTP/1.1 200 OK');
        CloseConnection($con);
    }
    //show an authors works interleaved
    else if (isset($_GET['author']))
    {
        $author =$_GET['author'];
        $con = OpenConnection();

        $query = "SELECT documentlist,movielist FROM authors where name='".$author."';";
        $result = mysqli_query($con, $query);

        if(mysqli_num_rows($result)>0){
            $row = mysqli_fetch_array($result);
            $docs = explode(',', $row['documentlist']);
            $movies = explode(',', $row['movielist']);
            $doc_list = array();
            $movie_list = array();
            foreach ($docs as $doc){
                $document_query = "SELECT * FROM documents WHERE name='".$doc."';";
//                echo $document_query;
                $result1 = mysqli_query($con, $document_query);
                array_push($doc_list, mysqli_fetch_array($result1));
            }
            foreach ($movies as $movie){
                $movie_query = "SELECT * FROM movies WHERE title='".$movie."';";
                $result2 = mysqli_query($con, $movie_query);
                array_push($movie_list, mysqli_fetch_array($result2));
            }
            echo "<table>";
            echo "<tr>";
            echo "<th>Name/Title</th>";
            echo "<th>Contents/Duration</th>";
            echo "</tr>";
            $n = sizeof($doc_list);
            $m = sizeof($movie_list);
            /*echo $n;
            echo $m;*/
            $i = 0;
            $j = 0;
            $docTurn = true;
            while ($i < $n and $j < $m){
                if($docTurn and $i < $n){
                    $docTurn = false;
                    echo "<tr>";
                    echo "<td>" . $doc_list[$i]['name'] . "</td>";
                    echo "<td>" . $doc_list[$i]['contents'] . "</td>";
                    echo "</tr>";
                    $i++;
                }
                if(!$docTurn and $i < $n){
                    $docTurn = true;
                    echo "<tr>";
                    echo "<td>" . $movie_list[$j]['title'] . "</td>";
                    echo "<td>" . $movie_list[$j]['duration'] . "</td>";
                    echo "</tr>";
                    $j++;
                }

            }
            while ($i < $n){
                echo "<tr>";
                echo "<td>" . $doc_list[$i]['name'] . "</td>";
                echo "<td>" . $doc_list[$i]['contents'] . "</td>";
                echo "</tr>";
                $i++;
            }
            while($j < $m){
                echo "<tr>";
                echo "<td>" . $movie_list[$j]['title'] . "</td>";
                echo "<td>" . $movie_list[$j]['duration'] . "</td>";
                echo "</tr>";
                $j++;
            }
            echo "</table>";
        }
        header('HTTP/1.1 200 OK');
        CloseConnection($con);
    }
    else
    {
        $con = OpenConnection();
        $max =0 ;
        $maxName ="Empty";
        $maxContent ="Empty Content";

        $query_docs = "SELECT * FROM documents";
        $result1 = mysqli_query($con, $query_docs);
        if(mysqli_num_rows($result1)>0){
            while ($row = mysqli_fetch_array($result1)){
                $docname = $row['name'];
                $contents = $row['contents'];
                $query_authors = "SELECT * FROM authors where documentlist LIKE '%".$docname ."%';";
                $result2 = mysqli_query($con, $query_authors);
                $crt = mysqli_num_rows($result2);
                if( $crt > $max){
                    $max = $crt;
                    $maxName = $docname;
                    $maxContent = $contents;
                }

            }
        }
        echo '<p>'."Most authored is ".$maxName." With ".$max." Authors and contents: ".$maxContent.'</p>';
        header('HTTP/1.1 200 OK');
        CloseConnection($con);
    }

} catch (Exception $e) {
    header('HTTP/1.1 500 INTERNAL_SERVER_ERROR');
    exit;
}
