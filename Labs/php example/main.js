let names = [];
let descriptions = [];
let values = [];


function showMostAuthorsDocument(){
    $.get("back.php",
        {},
        function (data, status) {
            $("#allItems").html(data);
        });
}

function showMatches(userId){
    $.get("back.php", { author: userId},
        function (data, status) {
            $("#documents").html(data);
        });
}

function add(){
    $.get("back.php", { name: $("#doc-name").val(), contents: $("#content-input").val() },
        function (data, status) {
            $("#subscribe-message").html(data);
        });
}

