﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace A9._ASP.NET.Models {
    public class User {
        // needed when I receive the new/updated user
        public User() { }
        public User(String name, String username, String password,
                    int age, String role, String profile, String email, String webpage) {
            this.name = name;
            this.username = username;
            this.password = password;
            this.age = age;
            this.role = role;
            this.profile = profile;
            this.email = email;
            this.webpage = webpage;
        }
        public int id { get; set; }
        public String name { get; set; }
        public String username { get; set; }
        public String password { get; set; }
        public int age { get; set; }
        public String role { get; set; }
        public String profile { get; set; }
        public String email { get; set; }
        public String webpage { get; set; }
    }
}
