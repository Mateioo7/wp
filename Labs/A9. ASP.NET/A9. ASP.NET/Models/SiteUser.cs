﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace A9._ASP.NET.Models {
    public class SiteUser {
        public int id { get; set; }
        public String username { get; set; }
        public String password { get; set; }
    }
}
