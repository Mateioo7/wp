﻿$(document).ready(function () {
    $('#login-form').validate({
        rules: {
            username: {
                required: true,
                minlength: 5
            },
            password: {
                required: true,
                minlength: 5
            }
        },
        messages: {
            username: {
                required: "Please enter a username",
                minlength: "Username must be at least 5 characters long"
            },
            password: {
                required: "Please enter a password",
                minlength: "Password must be at least 5 characters long"
            }
        }
    });

});