﻿using A9._ASP.NET.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace A9._ASP.NET.Data {
    public class DBWpContext : DbContext {
        public DBWpContext(DbContextOptions options) : base(options) {}
        // the field name has to be exactly like the table name from db
        public DbSet<User> User { get; set; }
        public DbSet<SiteUser> Site_User { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder) {
            modelBuilder.Entity<User>()
                .Property(user => user.id)
                .ValueGeneratedOnAdd();
        }
    }
}
