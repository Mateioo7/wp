﻿using A9._ASP.NET.Data;
using A9._ASP.NET.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace A9._ASP.NET.Controllers {
    [Route("[controller]")]
    public class UsersController : Controller {
        private readonly DBWpContext _context;
        public UsersController(DBWpContext context) {
            _context = context;
        }

        [HttpGet("")]
        public IActionResult RedirectToDashBoard() {
            if (HttpContext.Session.GetString("username") == null) {
                return Redirect("~/account/login");
            }

            return Redirect("~/users/dashboard");
        }

        [HttpGet("dashboard")]
        public IActionResult Dashboard() {
            if (HttpContext.Session.GetString("username") == null) {
                return Redirect("~/account/login");
            }

            return View("Dashboard", _context.User.ToList());
        }

        [HttpGet("add")]
        public IActionResult AddGet() {
            if (HttpContext.Session.GetString("username") == null) {
                return Redirect("~/account/login");
            }

            return View("Add");
        }

        [HttpPost("add")]
        public IActionResult AddPost(User newUser) {
            if (HttpContext.Session.GetString("username") == null) {
                return Redirect("~/account/login");
            }

            _context.Add(newUser);
            _context.SaveChanges();

            return Redirect("~/users/dashboard");
        }

        [HttpGet("get/role/")]
        public IActionResult GetByRole(String role) {
            if (HttpContext.Session.GetString("username") == null) {
                return Redirect("~/account/login");
            }

            return View("Dashboard", _context.User.Where(user => user.role == role).ToList());
        }

        [HttpGet("get/name/")]
        public IActionResult GetByName(String name) {
            if (HttpContext.Session.GetString("username") == null) {
                return Redirect("~/account/login");
            }

            return View("Dashboard", _context.User.Where(user => EF.Functions.Like(user.name, "%" + name + "%")).ToList());
        }

        [HttpGet("update/{id}")]
        public IActionResult UpdateGet(int id) {
            if (HttpContext.Session.GetString("username") == null) {
                return Redirect("~/account/login");
            }

            var user = _context.User.First(user => user.id == id);
            return View("Update", user);
        }

        [HttpPost("update")]
        public IActionResult UpdatePost(User updatedUser) {
            if (HttpContext.Session.GetString("username") == null) {
                return Redirect("~/account/login");
            }

            // obs: username field is unique
            User user = _context.User.FirstOrDefault(user => user.username == updatedUser.username);
            user.name = updatedUser.name;
            user.username = updatedUser.username;
            user.password = updatedUser.password;
            user.age = updatedUser.age;
            user.role = updatedUser.role;
            user.profile = updatedUser.profile;
            user.email = updatedUser.email;
            user.webpage = updatedUser.webpage;

            _context.SaveChanges();

            return Redirect("~/users/dashboard");
        }

        [HttpPost("delete/{id}")]
        public IActionResult Delete(int id) {
            if (HttpContext.Session.GetString("username") == null) {
                return Redirect("~/account/login");
            }

            User user = _context.User.FirstOrDefault(user => user.id == id);
            _context.User.Remove(user);
            _context.SaveChanges();

            return Redirect("~/users/dashboard");
        }
    }
}
