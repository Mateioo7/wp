﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using A9._ASP.NET.Data;
using A9._ASP.NET.Models;

namespace A9._ASP.NET.Controllers {
    [Route("[controller]")]
    public class AccountController : Controller {
        private readonly DBWpContext _context;
        public AccountController(DBWpContext context) {
            _context = context;
        }

        [HttpGet("login")]
        public IActionResult LoginGet() {
            if (HttpContext.Session.GetString("username") != null)
                return Redirect("~/users/dashboard");

            return View("Login");
        }

        [HttpPost("login")]
        public IActionResult LoginPost(String username, String password) {
            bool validCredentials = _context.Site_User.Any(user => user.username == username && user.password == password);

            if (validCredentials) {
                HttpContext.Session.SetString("username", username);
                return Redirect("~/users/dashboard");
            }
            else {
                return View("Login", "Username or password is invalid.");
            }
        }

        [HttpGet("logout")]
        public IActionResult Logout() {
            HttpContext.Session.Clear();
            return Redirect("~/account/login");
        }
    }
}
