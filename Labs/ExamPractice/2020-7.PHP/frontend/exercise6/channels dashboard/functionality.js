$(document).ready(function () {
    let table = $('#table');
    table.hide();

    $("#owner-submit").click(function() {
        let parameters = {};
        parameters['id'] = $("#owner").val();

        $.getJSON(
            "http://localhost/projects/ExamPractice/2020-7.PHP/backend/exercise6/get-channels.php",
            parameters,
            populateTable
        );
    })

    $("#subscribe-submit").click(function() {
        let parameters = {};
        parameters['id'] = $("#owner").val();

        $.getJSON(
            "http://localhost/projects/ExamPractice/2020-7.PHP/backend/exercise6/get-person-name.php",
            parameters,
            callback
        );
    })

    function callback(data) {
        let parameters = {};
        parameters['name'] = data;

        $.getJSON(
            "http://localhost/projects/ExamPractice/2020-7.PHP/backend/exercise6/get-channels-subscription.php",
            parameters,
            populateTable
        );
    }

    function populateTable(data) {
        $("#table > tbody").remove()

        let trHTML = '<tbody>';
        $.each(data, function (i, item) {
            trHTML +=
                '<tr>' +
                '<td>' + item.id + '</td>' +
                '<td>' + item.ownerId + '</td>' +
                '<td>' + item.name + '</td>' +
                '<td>' + item.description + '</td>' +
                '<td>' + item.subscribers + '</td>' +
                '</tr>';
        });
        trHTML += '<tbody>'

        table.append(trHTML);
        table.show();
    }

});
