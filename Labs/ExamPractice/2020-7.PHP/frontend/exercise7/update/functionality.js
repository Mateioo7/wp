$(document).ready(function () {
    let parameters = {}
    parameters['id'] = getUrlParameterValue('id');

    $.getJSON(
        "http://localhost/projects/ExamPractice/2020-7.PHP/backend/get-document.php",
        // /home/matei/Facultate/wp/Labs/ExamPractice/2020-7.PHP/backend/get-document.php
        parameters,
        autocompleteInputs
    );

    function autocompleteInputs(data) {
        $("#id-input").val(data.id);
        $("#website-input").val(data.website_id);
        $("#name-input").val(data.name);
        $("#keyword1-input").val(data.keyword1);
        $("#keyword2-input").val(data.keyword2);
        $("#keyword3-input").val(data.keyword3);
        $("#keyword4-input").val(data.keyword4);
        $("#keyword5-input").val(data.keyword5);
    }

    function getUrlParameterValue(paramName) {
        let searchParams = new URLSearchParams(window.location.search);
        return searchParams.get(paramName)
    }

    $("#submit").click(function () {
        $.ajax({
            type: "POST",
            url: "http://localhost/projects/ExamPractice/2020-7.PHP/backend/update-document.php",
            data: {
                id: $("#id-input").val(),
                keyword1: $("#keyword1-input").val(),
                keyword2: $("#keyword2-input").val(),
                keyword3: $("#keyword3-input").val(),
                keyword4: $("#keyword4-input").val(),
                keyword5: $("#keyword5-input").val(),
            },
            success: reload
        });

        function reload() {
            location.reload();
        }
    });
});
