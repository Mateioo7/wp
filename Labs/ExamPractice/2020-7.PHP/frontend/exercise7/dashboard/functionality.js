$(document).ready(function () {
    let websiteTable = $('#website-table');

    websiteTable.hide();

    $.getJSON(
        "http://localhost/projects/ExamPractice/2020-7.PHP/backend/exercise7/get-websites-with-documents-number.php",
        populateTable
    );

    function populateTable(data) {
        $("#website-table > tbody").remove()

        let trHTML = '<tbody>';
        $.each(data.one, function (i, item) {
            trHTML +=
                '<tr>' +
                '<td>' + item.id + '</td>' +
                '<td>' + item.url + '</td>' +
                '<td>' + item.count + '</td>' +
                '</tr>';
        });

        // for (const x of data.one) { console.log(x.id); }

        $.each(data.two, function (i, item) {
            trHTML +=
                '<tr>' +
                '<td>' + item.id + '</td>' +
                '<td>' + item.url + '</td>' +
                '<td>' + item.count + '</td>' +
                '</tr>';
        });
        trHTML += '<tbody>'

        websiteTable.append(trHTML);
        websiteTable.show();
    }

});
