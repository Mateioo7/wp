$(document).ready(function () {
    let usersTable = $('#users-table');

    usersTable.hide();

    $(document).on('click', '.edit-input', function (event) {
        const elementId = event.target.id;
        let id = elementId.split('-')[1];
        window.location.href = 'http://localhost/projects/A6.Php,AJAX,JSON/src/update/page.html?id=' + id;
    });

    $(document).on('click', '.remove-input', function (event) {
        let elementId = event.target.id;
        let id = elementId.split('-')[1];

        $.ajax({
            type: "POST",
            url: "http://localhost/projects/A6.Php,AJAX,JSON/src/backend/remove-user.php",
            data: {
                id: id
            },
            success: showStatus
        });

        function showStatus(data) {
            alert(data);
        }
    });

    $("#submit").click(function () {
        let parameters = {}
        parameters['name'] = $("#input").val();
        parameters["limit"] = 20;

        $.getJSON(
            "http://localhost/projects/A6.Php,AJAX,JSON/src/backend/get-users-with-name.php",
            parameters,
            populateTable
        );
    });

    function populateTable(data) {
        $("#users-table > tbody").remove()

        let trHTML = '<tbody>';
        $.each(data, function (i, item) {
            trHTML +=
                '<tr>' +
                '<td>' + item.name + '</td>' +
                '<td>' + item.username + '</td>' +
                '<td>' + item.password + '</td>'+
                '<td>' + item.age + '</td>' +
                '<td>' + item.role + '</td>' +
                '<td>' + item.profile + '</td>' +
                '<td>' + item.email + '</td>' +
                '<td>' + item.webpage + '</td>' +
                '<td><input id="edit-' + item.id + '" class="edit-input" type="submit" value="Update"></td>' +
                '<td><input id="remove-' + item.id + '" class="remove-input" type="submit" value="Remove"></td>' +
                '</tr>';
        });
        trHTML += '<tbody>'

        usersTable.append(trHTML);
        usersTable.show();
    }

});
