$(document).ready(function () {
    let usersTable = $('#users-table');

    usersTable.hide();

    $("#submit").click(function () {
        let parameters = {}
        parameters["role"] = $("#input").val();
        parameters["limit"] = 20;

        $.getJSON(
            "http://localhost/projects/A6.Php,AJAX,JSON/src/backend/get-users-with-role.php",
            parameters,
            populateTable
        );
    });

    function populateTable(data) {
        $("#users-table > tbody").remove()

        let trHTML = '<tbody>';
        $.each(data, function (i, item) {
            trHTML +=
                '<tr>' +
                '<td>' + item.id + '</td>' +
                '<td>' + item.name + '</td>' +
                '<td>' + item.username + '</td>' +
                '<td>' + item.password + '</td>'+
                '<td>' + item.age + '</td>' +
                '<td>' + item.role + '</td>' +
                '<td>' + item.profile + '</td>' +
                '<td>' + item.email + '</td>' +
                '<td>' + item.webpage + '</td>' +
                '</tr>';
        });
        trHTML += '<tbody>'

        usersTable.append(trHTML);
        usersTable.show();
    }

});
