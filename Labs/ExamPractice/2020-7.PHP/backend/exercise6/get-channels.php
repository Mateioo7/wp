<?php
/**
 * @var $connection
 */
require '../connect-to-database.php';

$owner_id = $_GET["id"];

$query = "select * from channels where owner_id = ?";

$statement = $connection->prepare($query);
$statement->bind_param("i", $owner_id);
$statement->execute();
$statement->bind_result($id, $owner_id, $name, $description, $subscribers);

$channels = array();

while ($statement->fetch()) {
    $channel = new StdClass();

    $channel->id = $id;
    $channel->ownerId = $owner_id;
    $channel->name = $name;
    $channel->description = $description;
    $channel->subscribers = $subscribers;

    array_push($channels, $channel);
}

echo json_encode($channels);

$statement->close();
$connection->close();
