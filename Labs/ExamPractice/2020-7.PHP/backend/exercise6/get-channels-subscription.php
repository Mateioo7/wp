<?php
/**
 * @var $connection
 */
require '../connect-to-database.php';

$ownerName = $_GET["name"];

$query = "select * from channels where subscribers like ?";


$statement = $connection->prepare($query);

$nameParam = '%' . $ownerName . '%';
$statement->bind_param("s", $nameParam);
$statement->execute();
$statement->bind_result($id, $ownerId, $name, $description, $subscribers);

$channels = array();

while ($statement->fetch()) {
    $channel = new StdClass();

    $channel->id = $id;
    $channel->ownerId = $ownerId;
    $channel->name = $name;
    $channel->description = $description;
    $channel->subscribers = $subscribers;

    array_push($channels, $channel);
}

echo json_encode($channels);

$statement->close();
$connection->close();
