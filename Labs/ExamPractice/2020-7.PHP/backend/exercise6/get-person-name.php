<?php
/**
 * @var $connection
 */
require '../connect-to-database.php';

$owner_id = $_GET["id"];

$query = "select name from persons where id = ?";

$statement = $connection->prepare($query);
$statement->bind_param("i", $owner_id);
$statement->execute();
$statement->bind_result($name);
$statement->fetch();

echo json_encode($name);

$statement->close();
$connection->close();
