<?php
/**
 * @var $connection
 */
require '../connect-to-database.php';

$query = "select id, url, (select count(*) from documents where website_id = w.id) as count from websites w";


// SELECT MAX(y.num)
//  FROM (SELECT COUNT(*) AS num
//          FROM TABLE x) y
$statement = $connection->prepare($query);
$statement->execute();
$statement->bind_result($id, $url, $count);

$websites = array();

while ($statement->fetch()) {
    $website = new StdClass();

    $website->id = $id;
    $website->url = $url;
    $website->count = $count;

    array_push($websites, $website);
}

$data = array();
$data['one'] = $websites;
$data['two'] = $websites;

echo json_encode($data);

$statement->close();
$connection->close();
