<?php
/**
 * @var $connection
 */
require 'connect-to-database.php';

$idToFind = $_GET["id"];

$query = "SELECT id, website_id, name, keyword1, keyword2, keyword3, 
       keyword4, keyword5  FROM documents where id = ?";

$statement = $connection->prepare($query);
$statement->bind_param("i", $idToFind);
$statement->execute();
$statement->bind_result($id, $website_id, $name, $keyword1, $keyword2, $keyword3,
    $keyword4, $keyword5);
$statement->fetch();

$document = new StdClass();
$document->id = $id;
$document->website_id = $website_id;
$document->name = $name;
$document->keyword1 = $keyword1;
$document->keyword2 = $keyword2;
$document->keyword3 = $keyword3;
$document->keyword4 = $keyword4;
$document->keyword5 = $keyword5;

echo json_encode($document);

$statement->close();
$connection->close();
