<?php
/**
 * @var $connection
 */
require 'connect-to-database.php';

$keywords = $_POST["keywords"];
foreach($keywords as $keyword){
    echo $keyword;
}


$query = "SELECT id, website_id, name, keyword1, keyword2, keyword3, 
       keyword4, keyword5  FROM documents";

$statement = $connection->prepare($query);
$statement->bind_param("i", $idToFind);
$statement->execute();
$statement->bind_result($id, $website_id, $name, $keyword1, $keyword2, $keyword3,
    $keyword4, $keyword5);


$documents = array();

while ($statement->fetch()) {
    $document = new StdClass();

    if ($keyword1)
    $document->id = $id;
    $document->website_id = $website_id;
    $document->name = $name;
    $document->keyword1 = $keyword1;
    $document->keyword2 = $keyword2;
    $document->keyword3 = $keyword3;
    $document->keyword4 = $keyword4;
    $document->keyword5 = $keyword5;

    array_push($documents, $document);
}
$document = new StdClass();
$document->id = $id;
$document->website_id = $website_id;
$document->name = $name;
$document->keyword1 = $keyword1;
$document->keyword2 = $keyword2;
$document->keyword3 = $keyword3;
$document->keyword4 = $keyword4;
$document->keyword5 = $keyword5;

echo json_encode($document);

$statement->close();
$connection->close();
