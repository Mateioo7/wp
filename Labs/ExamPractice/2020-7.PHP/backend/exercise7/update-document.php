<?php
/**
 * @var $connection
 */
require 'connect-to-database.php';

$id = $_POST["id"];
$keyword1 = $_POST["keyword1"];
$keyword2 = $_POST["keyword2"];
$keyword3 = $_POST["keyword3"];
$keyword4 = $_POST["keyword4"];
$keyword5 = $_POST["keyword5"];

$query = "update documents set 
            keyword1 = ?,
            keyword2 = ?,
            keyword3 = ?,
            keyword4 = ?,
            keyword5 = ?
        where id = ?";

$statement = $connection->prepare($query);
$statement->bind_param("sssssi", $keyword1,
    $keyword2, $keyword3, $keyword4, $keyword5, $id);

$statement->execute();

$statement->close();
$connection->close();








