package com.example.ExamJSP.services;


import com.example.ExamJSP.domain.User;

import java.sql.*;

public class LoginService {
    private Connection connection;

    public LoginService() throws SQLException, ClassNotFoundException {
        this.initializeConnection();
    }

    private void initializeConnection() throws ClassNotFoundException, SQLException {
        String url = "jdbc:mysql://localhost:3306";
        String user = "root";
        String password = "root";

        Class.forName("com.mysql.cj.jdbc.Driver");
        this.connection = DriverManager.getConnection(url, user, password);
    }

    public User checkLogin(String username, String password) throws SQLException {
        String sql = "select * from `web-programming`.jsp_user where username = ? and password = ?";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, username);
        statement.setString(2, password);

        ResultSet result = statement.executeQuery();
        User user = null;
        if (result.next()) {
            user = new User();
            user.setUsername(result.getString("username"));
            user.setPassword(result.getString("password"));
        }

        connection.close();
        return user;
    }
}
