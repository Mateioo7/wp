package com.example.ExamJSP.practice;

import com.example.ExamJSP.services.AnimalRepository;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "animalCreateController", value = "/animal-create")
public class AnimalCreateController extends HttpServlet {
    public void doPost(HttpServletRequest request, HttpServletResponse response) {
        if (request.getSession().getAttribute("username") == null) {
            try {
                response.sendRedirect("login.jsp");
                return;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            AnimalRepository animalRepository = new AnimalRepository();

            String species = request.getParameter("species");
            String age = request.getParameter("age");
            String name = request.getParameter("name");

            animalRepository.createAnimal(species, age, name);

            response.sendRedirect("animal-dashboard");
        } catch (SQLException | ClassNotFoundException | IOException throwable) {
            throwable.printStackTrace();
        }
    }
}
