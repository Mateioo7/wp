package com.example.ExamJSP.services;

import com.example.ExamJSP.domain.Animal;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class AnimalRepository {
    private Connection connection;

    public AnimalRepository() throws SQLException, ClassNotFoundException {
        this.initializeConnection();
    }

    private void initializeConnection() throws ClassNotFoundException, SQLException {
        String url = "jdbc:mysql://localhost:3306";
        String user = "root";
        String password = "root";

        Class.forName("com.mysql.cj.jdbc.Driver");
        this.connection = DriverManager.getConnection(url, user, password);
    }

    public List<Animal> getAllAnimals() throws SQLException {
        String sql = "select * from `web-programming`.animal";
        PreparedStatement statement = connection.prepareStatement(sql);
        ResultSet resultSet = statement.executeQuery();

        List<Animal> animalList = new ArrayList<>();
        while (resultSet.next()) {
            Animal animal = new Animal();
            animal.setId(Integer.parseInt(resultSet.getString("id")));
            animal.setSpecies(resultSet.getString("species"));
            animal.setAge(Integer.parseInt(resultSet.getString("age")));
            animal.setName(resultSet.getString("name"));

            animalList.add(animal);
        }

        return animalList;
    }

    public List<Animal> getAllAnimalsOlderThan(String age) throws SQLException {
        String sql = "select * from `web-programming`.animal where age > ?";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(1, Integer.parseInt(age));
        ResultSet resultSet = statement.executeQuery();

        List<Animal> animalList = new ArrayList<>();
        while (resultSet.next()) {
            Animal animal = new Animal();
            animal.setId(Integer.parseInt(resultSet.getString("id")));
            animal.setSpecies(resultSet.getString("species"));
            animal.setAge(Integer.parseInt(resultSet.getString("age")));
            animal.setName(resultSet.getString("name"));

            animalList.add(animal);
        }

        return animalList;
    }

    public Animal getAnimal(String id) throws SQLException {
        String sql = "select * from `web-programming`.animal where id = ?";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(1, Integer.parseInt(id));
        ResultSet resultSet = statement.executeQuery();

        Animal animal = null;
        while (resultSet.next()) {
            animal = new Animal();
            animal.setId(Integer.parseInt(resultSet.getString("id")));
            animal.setSpecies(resultSet.getString("species"));
            animal.setAge(Integer.parseInt(resultSet.getString("age")));
            animal.setName(resultSet.getString("name"));
        }

        return animal;
    }

    public void updateAnimal(String id, String species, String age, String name) throws SQLException {
        String sql = "update `web-programming`.animal set species = ?, age = ?, name = ? where id = ?";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, species);
        statement.setInt(2, Integer.parseInt(age));
        statement.setString(3, name);
        statement.setInt(4, Integer.parseInt(id));
        statement.executeUpdate();
    }

    public void deleteAnimal(String id) throws SQLException {
        String sql = "delete from `web-programming`.animal where id = ?";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(1, Integer.parseInt(id));
        statement.executeUpdate();
    }

    public void createAnimal(String species, String age, String name) throws SQLException {
        String sql = "insert into `web-programming`.animal (species, age, name) value (?, ?, ?)";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, species);
        statement.setInt(2, Integer.parseInt(age));
        statement.setString(3, name);
        statement.executeUpdate();
    }
}
