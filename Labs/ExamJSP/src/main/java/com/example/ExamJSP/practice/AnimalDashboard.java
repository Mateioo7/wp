package com.example.ExamJSP.practice;


import com.example.ExamJSP.domain.Animal;
import com.example.ExamJSP.services.AnimalRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@WebServlet(name = "animalDashboard", value = "/animal-dashboard")
public class AnimalDashboard extends HttpServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse response) {
        if (request.getSession().getAttribute("username") == null) {
            try {
                response.sendRedirect("login.jsp");
                return;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            List<Animal> animalList;
            if (request.getParameter("age") != null) {
                AnimalRepository animalRepository = new AnimalRepository();
                animalList = animalRepository.getAllAnimalsOlderThan(request.getParameter("age"));
            }
            else {
                AnimalRepository animalRepository = new AnimalRepository();
                animalList = animalRepository.getAllAnimals();
            }


            request.setAttribute("animalList", animalList);

            String username = (String) request.getSession().getAttribute("username");
            request.setAttribute("username", username);

            request.getRequestDispatcher("animal-dashboard.jsp").forward(request, response);
        } catch (SQLException | ClassNotFoundException | IOException | ServletException throwable) {
            throwable.printStackTrace();
        }
    }
}
