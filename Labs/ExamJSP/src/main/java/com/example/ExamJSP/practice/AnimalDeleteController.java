package com.example.ExamJSP.practice;


import com.example.ExamJSP.services.AnimalRepository;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;

@WebServlet(name = "animalDeleteController", value = "/animal-delete")
public class AnimalDeleteController extends HttpServlet {
    public void doPost(HttpServletRequest request, HttpServletResponse response) {
        try {
            AnimalRepository animalRepository = new AnimalRepository();
            animalRepository.deleteAnimal(request.getParameter("id"));
        } catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
        }
    }
}
