package com.example.ExamJSP.lab;


import com.example.ExamJSP.services.ImageService;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "loadImageServlet", value = "/images/*")
public class LoadImageServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        if (session.getAttribute("username") == null) {
            try {
                response.sendRedirect("login.jsp");
                return;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        String imageId = request.getPathInfo().substring(1); // the 1 (id) from: ".../images/1"

        try {
            ImageService imageService = new ImageService();
            byte[] content = imageService.getImageContent(imageId);
            response.setContentLength(content.length);
            response.getOutputStream().write(content);

        } catch (SQLException | ClassNotFoundException | IOException throwable) {
            throwable.printStackTrace();
        }
    }
}
