package com.example.ExamJSP.authentication;


import com.example.ExamJSP.domain.User;
import com.example.ExamJSP.services.LoginService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "loginServlet", value = "/login")
public class LoginServlet extends HttpServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getSession().getAttribute("username") != null) {
            try {
                response.sendRedirect("dashboard");
                return;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        request.getRequestDispatcher("login.jsp").forward(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        try {
            LoginService loginService = new LoginService();
            User user = loginService.checkLogin(username, password);

            if (user != null) {
                HttpSession session = request.getSession();
                session.setAttribute("username", username);

                // below was for lab
//                response.sendRedirect("dashboard");
                response.sendRedirect("animal-dashboard");
            }
            else {
                request.setAttribute("message", "Invalid email/password.");

                request.getRequestDispatcher("login.jsp").forward(request, response);
            }

        } catch (SQLException | ClassNotFoundException | ServletException throwable) {
            throwable.printStackTrace();
        }
    }
}
