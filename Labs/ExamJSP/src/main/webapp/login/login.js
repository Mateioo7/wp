$(document).ready(function() {
    $("#loginForm").validate({
        rules: {
            username: {
                required: true,
                minlength: 5
            },

            password: {
                required: true,
                minlength: 5
            }
        },

        messages: {
            username: {
                required: "Please enter username.",
                minlength: "Please enter at least 5 characters."
            },

            password: {
                required: "Please enter password.",
                minlength: "Please enter at least 5 characters."
            }
        }
    });

});
