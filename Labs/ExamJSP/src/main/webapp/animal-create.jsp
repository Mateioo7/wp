<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Create Animal</title>
</head>
<body>

<form action="animal-create" method="post">
    <label for="species">Species:</label>
    <input type="text" id="species" name="species"><br>

    <label for="age">Age:</label>
    <input type="text" id="age" name="age"><br>

    <label for="name">Name:</label>
    <input type="text" id="name" name="name"><br>

    <input type="submit" value="Create">
</form>

<form id="create-form" action="animal-create" method="post">
    <label for="species-form">Species:</label>
    <input type="text" id="species-form" name="species"><br>

    <input type="hidden" id="age-form" name="age" value="">

    <label for="name-form">Name:</label>
    <input type="text" id="name-form" name="name"><br>

    <input type="submit" value="Create">
</form>

<script>
    // db date: YYYY-MM-DD
    // db datetime: YYYY-MM-DD hh:mm:ss
    // const currentMySQLDate = new Date().toISOString().slice(0, 10);
    // const currentMySQLDateTime = new Date().toISOString().slice(0, 10) + ' ' + new Date().toLocaleTimeString('en-GB');
    (function() {
        document.getElementById("age-form").value = new Date().getHours();
    })();
    // <form onsubmit="createAnimal()">....
    // function createAnimal() {
    //     // alert(new Date().getHours());
    //     const params = {'species': $("#species-form").val(),
    //         'age': $("#age-form").val(),
    //         'name': $("#name-form").val()}
    //
    //     $.post('animal-create', params, function () {
    //         window.location.href = "http://localhost:8080/Gradle___com_example___ExamJSP_1_0_SNAPSHOT_war/animal-dashboard";
    //     });
    // }
</script>
</body>
</html>
