<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Animals</title>

    <script src="plugins/jquery-3.6.0.js"></script>
    <script src="plugins/jquery-validation/dist/jquery.validate.js"></script>
    <script src="animal-dashboard/code.js"></script>
</head>
<body>
<h3>Hello ${username}</h3>

<a href="animal-create.jsp">Create animal</a>

<form action="animal-dashboard" method="get">
    <label for="age-input">Age: </label>
    <input id="age-input" type="text" name="age">
    <input type="submit" value="Get animals older than above age">
</form>

<form action="animal-dashboard" method="get">
    <input type="submit" value="Get all animals">
</form>

<table>
    <thead>
    <tr>
        <th>Species</th>
        <th>Age</th>
        <th>Name</th>
        <th></th>
        <th></th>
    </tr>
    </thead>

    <tbody>
    <c:forEach items="${animalList}" var="animal">
        <tr>
            <td>
                <c:out
                        value="${animal.species}">
                </c:out>
            </td>
            <td>
                <c:out
                        value="${animal.age}">
                </c:out>
            </td>
            <td>
                <c:out
                        value="${animal.name}">
                </c:out>
            </td>
            <td>
                <form style="margin: 0" method="get" action="animal-update">
                    <input type="hidden" name="id" value="${animal.id}">
                    <input type="submit" value="Update">
                </form>
            </td>
            <td>
                <button onclick="deleteAnimal(${animal.id})">Delete</button>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>

<script>
    // self executing function here
    // (function() {
    // })();
</script>
</body>
</html>
