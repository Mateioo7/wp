<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Dashboard</title>

    <script src="plugins/jquery-3.6.0.js"></script>
    <script src="plugins/jquery-validation/dist/jquery.validate.js"></script>
    <script src="dashboard/dashboard.js"></script>

    <link rel="stylesheet" href="${pageContext.request.contextPath}/dashboard/dashboard.css">
</head>

<body>

<div style="text-align: center">
    <div>
        <a href="logout">Logout</a>
        <a href="dashboard">Dashboard</a>
        <h3>Welcome back to Images Dashboard, ${username}</h3>
    </div>

    <form action="best-images" method="get">
        <label>Give a number:</label>
        <input type="number" min="1" name="topValue">
        <button type="submit">Show best images</button>
    </form>

    <form id="fileUpload" action="upload-image" method="post" enctype="multipart/form-data">
        <input type="file" name="file" />
        <input type="submit" value="Upload image"/>
    </form>

    <table>
        <thead>
            <tr>
                <th>Image</th>
                <th>Author</th>
                <th>Score</th>
                <th>Number of votes</th>
            </tr>
        </thead>

        <tbody>
            <c:forEach items="${imageList}" var="animal">
                <tr>
                    <td>
                        <img style="height: 200px; width: 400px" src=
                             <c:out
                                 value="${pageContext.request.contextPath}/images/${animal.id}">
                             </c:out>>
                    </td>
                    <td>
                        <c:out
                            value="${animal.author}">
                        </c:out>
                    </td>
                    <td>
                        <c:out
                             value="${animal.score}">
                        </c:out>
                    </td>
                    <td>
                        <c:out
                                value="${animal.votesNumber}">
                        </c:out>
                    </td>
                    <td>
                        <c:if test="${animal.author != username}">
                            <input id="vote-input-${animal.id}" type="number" min="1" max="10">
                            <button id="vote-submit-${animal.id}" class="vote-submit">Vote</button>
                        </c:if>
                    </td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
</div>

</body>
</html>
