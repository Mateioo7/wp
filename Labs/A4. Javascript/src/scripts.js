function showMainMenu() {
    document.getElementById("categories-main-menu").style.display = "block";
}

function hideMainMenu() {
    document.getElementById("categories-main-menu").style.display = "none";
}

function showSubmenuById(elementId) {
    hideAllSubmenus();
    document.getElementById(elementId).style.display = "block";
}

function hideAllSubmenus() {
    const allDropdowns = document.getElementsByClassName('dropdown-item-content');
    for (let i = 0; i < allDropdowns.length; i++) {
        allDropdowns[i].style.display = "none";
    }
}

function hideElementById(elementId) {
    document.getElementById(elementId).style.display = "none"
}