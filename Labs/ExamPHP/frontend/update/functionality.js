$(document).ready(function () {
    let parameters = {}
    parameters['id'] = getUrlParameterValue('id');

    $.getJSON(
        "http://localhost/projects/ExamPHP/backend/get-entity-by-id.php",
        parameters,
        autocompleteInputs
    );

    function autocompleteInputs(data) {
        $("#id-submit").val(data.id);
        $("#id-form").val(data.id);
        $("#url-input").val(data.url);
        $("#url-form").val(data.url);
        // $("#username-input").val(data.username);
        // $("#password-input").val(data.password);
        // $("#age-input").val(data.age);
        // $("#role-input").val(data.role);
        // $("#profile-input").val(data.profile);
        // $("#email-input").val(data.email);
        // $("#webpage-input").val(data.webpage);
    }

    function getUrlParameterValue(paramName) {
        let searchParams = new URLSearchParams(window.location.search);
        return searchParams.get(paramName)
    }

    $("#basic-submit").click(function () {
        $.ajax({
            type: "POST",
            url: "http://localhost/projects/ExamPHP/backend/update-entity.php",
            data: {
                id: $("#id-submit").val(),
                url: $("#url-input").val(),
                // username: $("#username-input").val(),
                // password: $("#password-input").val(),
                // age: $("#age-input").val(),
                // role: $("#role-input").val(),
                // profile: $("#profile-input").val(),
                // email: $("#email-input").val(),
                // webpage: $("#webpage-input").val()
            },
            // success: showStatus
        });

        function showStatus() {
            alert("Entity added");
        }
    });
});
