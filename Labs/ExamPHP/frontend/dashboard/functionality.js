$(document).ready(function () {
    let entityTable = $('#entity-table');
    entityTable.hide();

    // automatic fetch
    // $.getJSON(
    //     "http://localhost/projects/ExamPHP/backend/get-entities.php",
    //     populateTable
    // );

    // fetch on submit input click
    $("#basic-fetch-input").click(function () {
        $.getJSON(
            "http://localhost/projects/ExamPHP/backend/get-entities.php",
            populateTable
        );
    });

    // fetch on submit basic form
    $("#basic-fetch-form").submit(function (e) {
        // prevent form submit from reloading page
        e.preventDefault();

        $.getJSON(
            "http://localhost/projects/ExamPHP/backend/get-entities.php",
            populateTable
        );
    });

    // fetch on submit advanced form
    $("#advanced-fetch-form").submit(function (e) {
        // prevent form submit from reloading page
        e.preventDefault();

        const parameters = {}
        parameters["number"] = $("#documents-number").val();

        // var d = new Date();
        // var strDate = d.getFullYear() + "/" + (d.getMonth()+1) + "/" + d.getDate();

        $.getJSON(
            "http://localhost/projects/ExamPHP/backend/get-entities.php",
            parameters,
            populateTable
        );
    });

    function populateTable(data) {
        $("#entity-table > tbody").remove()

        let trHTML = '<tbody>';
        $.each(data, function (i, item) {
            trHTML +=
                '<tr>' +
                '<td>' + item.id + '</td>' +
                '<td>' + item.url + '</td>' +
                '<td>' + item.count + '</td>' +
                '<td>' +
                    '<a href="../update/page.html?id=' + item.id + '">Update</a>' +
                '</td>' +
                '<td>' +
                    // <form action="http://localhost/projects/ExamPHPAngular/backend/delete-entity.php" method="post"
                //               onsubmit="return confirm('Confirm deletion?');">
                //           <input type="hidden" name="id" value="item.id" />
                //           <input type="submit" value="Delete" />
                //         </form>
                    '<a onclick="return confirm(\'Confirm deletion?\');" href="../delete/page.html?id=' + item.id + '">Delete</a>' +
                '</td>' +
                '</tr>';
        });
        trHTML += '<tbody>'

        entityTable.append(trHTML);
        entityTable.show();
    }
});
