<?php
session_start();
/**
 * @var $connection
 */
require 'database-connection.php';



$array = $_GET["array"];

$query = "select * from documents2";
$statement = $connection->prepare($query);
$statement->execute();
$statement->bind_result($id, $name, $contents);

$entities = array();
while ($statement->fetch()) {
    $entity = new StdClass();

    $entity->id = $id;
    $entity->name = $name;
    $entity->contents = $contents;

    array_push($entities, $entity);
}

echo json_encode($entities);

$statement->close();
$connection->close();
