<?php
session_start();
/**
 * @var $connection
 */
require 'database-connection.php';

$query = "select * from auction where user = '".$_SESSION['name']."'";
$statement = $connection->prepare($query);
$statement->execute();
$statement->bind_result($id, $user, $category_id, $description, $date);

$entities = array();
while ($statement->fetch()) {
    $entity = new StdClass();

    $entity->id = $id;
    $entity->user = $user;
    $entity->category_id = $category_id;
    $entity->description = $description;
    $entity->date = $date;

    array_push($entities, $entity);
}

echo json_encode($entities);

$statement->close();
$connection->close();
