$(document).ready(function () {
    let parameters = {}
    parameters['id'] = getUrlParameterValue('id');

    $.getJSON(
        "http://localhost/projects/ExamPHP/backend-exam/get-auction-with-id.php",
        parameters,
        autocompleteInputs
    );

    function autocompleteInputs(data) {
        $("#id-input").val(data.id);
        $("#user-input").val(data.user);
        $("#category-input").val(data.category_id);
        $("#description-input").val(data.description);
        $("#date-input").val(data.date);
        // $("#username-input").val(data.username);
        // $("#password-input").val(data.password);
        // $("#age-input").val(data.age);
        // $("#role-input").val(data.role);
        // $("#profile-input").val(data.profile);
        // $("#email-input").val(data.email);
        // $("#webpage-input").val(data.webpage);
    }

    function getUrlParameterValue(paramName) {
        let searchParams = new URLSearchParams(window.location.search);
        return searchParams.get(paramName)
    }

    $("#basic-submit").click(function () {
        $.ajax({
            type: "POST",
            url: "http://localhost/projects/ExamPHP/backend-exam/update-auction.php",
            data: {
                id: $("#id-input").val(),
                category_id: $("#category-input").val(),
                description: $("#description-input").val(),
                date: new Date().toISOString().slice(0, 10),
            }
        });

        goHome();

        function goHome() {
            window.location.href = "http://localhost/projects/ExamPHP/frontend-exam/index.php";
        }
    });
});
