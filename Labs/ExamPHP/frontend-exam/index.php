<?php
session_start();
if (!isset($_SESSION['name'])) {
    header("Location: http://localhost/projects/ExamPHP/frontend-exam/login.php");
}
?>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Entity Dashboard</title>
    <!--  <link rel="stylesheet" href="../../../resources/style.css">-->
    <link rel="stylesheet" href="dashboard/style.css">
    <script src="https://code.jquery.com/jquery-latest.min.js"></script>
    <script src="index.js"></script>
</head>

<body>

<a href="login.php">Login</a>
<a href="../backend-exam/logout.php">Logout</a>

<?php
if (isset($_SESSION['name']))
    echo '<h3>Hi ' . $_SESSION['name'] . '</h3>';
?>


<input id="id-input" type="hidden">

<label>Give category name: </label>
<input id="category-input" type="text" name="category_name"><br><br>

<label>Give description: </label>
<input id="description-input" type="text" name="description"><br><br>

<input id="basic-submit" type="submit" value="Create auction">

<br><br>
<div>Auctions:</div>
<div id="container">
    <table id="auction-table">
        <thead>
        <tr>
            <th>Id</th>
            <th>User</th>
            <th>Category id</th>
            <th>Description</th>
            <th>Date</th>
        </tr>
        </thead>
    </table>
</div>


<br>
<br>

<!--<input id="latest-input" type="submit" value="Get latest auction(s) added">-->

<div>Latest auction(s) added:</div>
<table id="latest-table">
    <thead>
    <tr>
        <th>Id</th>
        <th>User</th>
        <th>Category id</th>
        <th>Description</th>
        <th>Date</th>
    </tr>
    </thead>
</table>

</body>
</html>


