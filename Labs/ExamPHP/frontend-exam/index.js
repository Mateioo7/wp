$(document).ready(function () {
    // db date: YYYY-MM-DD
    // db datetime: YYYY-MM-DD hh:mm:ss
    // const currentMySQLDate = new Date().toISOString().slice(0, 10);
    // const currentMySQLDateTime = new Date().toISOString().slice(0, 10) + ' ' + new Date().toLocaleTimeString('en-GB');

    let auctionTable = $('#auction-table');
    auctionTable.hide();

    let latestTable = $('#latest-table');
    latestTable.hide();

    // automatic fetch
    $.getJSON(
        "http://localhost/projects/ExamPHP/backend-exam/get-auctions.php",
        populateTable2
    );

    function populateTable2(data) {
        $("#auction-table > tbody").remove()

        let trHTML = '<tbody>';
        $.each(data, function (i, item) {
            trHTML +=
                '<tr>' +
                    '<td>' + item.id + '</td>' +
                    '<td>' + item.user + '</td>' +
                    '<td>' + item.category_id + '</td>' +
                    '<td>' + item.description + '</td>' +
                    '<td>' + item.date + '</td>' +
                    '<td>' +
                        '<a href="update/page.html?id=' + item.id + '">Update</a>' +
                    '</td>' +
                '</tr>';
        });
        trHTML += '<tbody>'

        auctionTable.append(trHTML);
        auctionTable.show();
    }

    $("#basic-submit").click(function () {
        $.ajax({
            type: "POST",
            url: "http://localhost/projects/ExamPHP/backend-exam/create-auction.php",
            data: {
                category_name: $("#category-input").val(),
                description: $("#description-input").val(),
                date: new Date().toISOString().slice(0, 10),
            },
            success: refresh
        });

        function refresh() {
            window.location.reload();
        }
    });

    function getLatestAuctions() {
        console.log('fetching latest auction(s)...');
        $.getJSON(
            "http://localhost/projects/ExamPHP/backend-exam/get-last-auction.php",
            populateTable3
        );
    }

    getLatestAuctions()

    setInterval(getLatestAuctions,10000);

    $("#latest-input").click(function () {
        getLatestAuctions();
    });

    function populateTable3(data) {
        $("#latest-table > tbody").remove()

        let trHTML = '<tbody>';
        $.each(data, function (i, item) {
            trHTML +=
                '<tr>' +
                '<td>' + item.id + '</td>' +
                '<td>' + item.user + '</td>' +
                '<td>' + item.category_id + '</td>' +
                '<td>' + item.description + '</td>' +
                '<td>' + item.date + '</td>' +
                '</tr>';
        });
        trHTML += '<tbody>'

        latestTable.append(trHTML);
        latestTable.show();
    }
});
