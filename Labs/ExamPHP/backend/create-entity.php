<?php
/**
 * @var $connection
 */
require 'database-connection.php';

$url = $_POST["url"];

$query = "insert into websites (url)
        values (?)";

$statement = $connection->prepare($query);
$statement->bind_param("s", $url);
$statement->execute();

$statement->close();
$connection->close();
