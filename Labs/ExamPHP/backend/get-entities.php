<?php
session_start();
/**
 * @var $connection
 */
require 'database-connection.php';

$documentsNumber = $_GET["number"];

if (isset($_GET["number"])) {
    $documentsNumber = $_GET["number"];

    $query = "select id, url, (select count(*) from documents where website_id = w.id)
    as count from websites w having count > ?";

    $statement = $connection->prepare($query);
    $statement->bind_param("i", $documentsNumber);
}
else {
    $nameParam = '%' . $_SESSION['name'] . '%';
//    $query = "select id, url, (select count(*) from documents where website_id = w.id) as count from websites w
//where url = ?";
    $query = "select id, url, (select count(*) from documents where website_id = w.id) as count from websites w
where url = ?";

    $statement = $connection->prepare($query);
    $nameParam = 'www.' . $_SESSION['name'] . '.com';
    $statement->bind_param("s", $nameParam);
}

$statement->execute();
$statement->bind_result($id, $url, $count);

$entities = array();
while ($statement->fetch()) {
    $entity = new StdClass();

    $entity->id = $id;
    $entity->url = $url;
    $entity->count = $count;

    array_push($entities, $entity);
}

$data = array();
$data['one'] = $entities;
$data['two'] = $entities;

// { [entity1, ..] }
// { one: [entitate1, ...], two: ... }

echo json_encode($data);

$statement->close();
$connection->close();
