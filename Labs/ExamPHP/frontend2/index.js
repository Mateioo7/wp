$(document).ready(function () {
    // db date: YYYY-MM-DD
    // db datetime: YYYY-MM-DD hh:mm:ss
    // const currentMySQLDate = new Date().toISOString().slice(0, 10);
    // const currentMySQLDateTime = new Date().toISOString().slice(0, 10) + ' ' + new Date().toLocaleTimeString('en-GB');

    let entityTable = $('#entity-table');
    entityTable.hide();

    let interleavedTable = $('#interleaved-table');
    interleavedTable.hide();

    let documentsTable = $('#documents-table');
    documentsTable.hide();

    // automatic fetch
    $.getJSON(
        "http://localhost/projects/ExamPHP/backend2/get-entities.php",
        populateTable
    );

    $.getJSON(
        "http://localhost/projects/ExamPHP/backend2/get-interleaved.php",
        populateTable3
    );

    $("#largest-input").click(function () {
        $.getJSON(
            "http://localhost/projects/ExamPHP/backend2/get-largest-document.php",
            populateTable2
        );
    });

    function populateTable2(data) {
        $("#documents-table > tbody").remove()

        let trHTML = '<tbody>';
        $.each(data, function (i, item) {
            trHTML +=
                '<tr>' +
                '<td>' + item.id + '</td>' +
                '<td>' + item.name + '</td>' +
                '<td>' + item.contents + '</td>' +
                '<td>' + item.count + '</td>' +
                '</tr>';
        });
        trHTML += '<tbody>'

        documentsTable.append(trHTML);
        documentsTable.show();
    }

    function populateTable3(data) {
        $("#interleaved-table > tbody").remove()

        let trHTML = '<tbody>';

        let docTurn = true;
        let i = 0, j = 0;
        while (i < data.documents.length && j < data.movies.length) {
            if (docTurn) {
                trHTML +=
                    '<tr>' +
                    '<td>Document id: ' + data.documents[i].id + '</td>' +
                    '<td>Name: ' + data.documents[i].name + '</td>' +
                    '<td>Contents: ' + data.documents[i].contents + '</td>' +
                    '</tr>';
                i++;
                docTurn = false;
            }
            else {
                trHTML +=
                    '<tr>' +
                    '<td>Movie id: ' + data.movies[j].id + '</td>' +
                    '<td>Title: ' + data.movies[j].title + '</td>' +
                    '<td>Duration: ' + data.movies[j].duration + '</td>' +
                    '</tr>';
                j++;
                docTurn = true;
            }
        }

        while (j < data.movies.length) {
            trHTML +=
                '<tr>' +
                '<td>Movie id: ' + data.movies[j].id + '</td>' +
                '<td>Title: ' + data.movies[j].title + '</td>' +
                '<td>Duration: ' + data.movies[j].duration + '</td>' +
                '</tr>';
            j++;
        }

        while (i < data.documents.length) {
            trHTML +=
                '<tr>' +
                '<td>Document id: ' + data.documents[i].id + '</td>' +
                '<td>Name: ' + data.documents[i].name + '</td>' +
                '<td>Contents: ' + data.documents[i].contents + '</td>' +
                '</tr>';
            i++;
        }

        trHTML += '<tbody>'

        interleavedTable.append(trHTML);
        interleavedTable.show();
    }

    // fetch on submit input click
    $("#basic-fetch-input").click(function () {
        $.getJSON(
            "http://localhost/projects/ExamPHP/backend2/get-entities.php",
            populateTable
        );
    });

    // fetch on submit basic form
    $("#basic-fetch-form").submit(function (e) {
        // prevent form submit from reloading page
        e.preventDefault();

        $.getJSON(
            "http://localhost/projects/ExamPHP/backend/get-entities.php",
            populateTable
        );
    });

    // fetch on submit advanced form
    $("#advanced-fetch-form").submit(function (e) {
        // prevent form submit from reloading page
        e.preventDefault();

        const parameters = {}
        // parameters["number"] = $("#documents-number").val();
        parameters['array'] = [1, 2, 3]

        // var d = new Date();
        // var strDate = d.getFullYear() + "/" + (d.getMonth()+1) + "/" + d.getDate();

        $.getJSON(
            "http://localhost/projects/ExamPHP/backend2/get-entities.php",
            parameters,
            populateTable
        );
    });

    function populateTable(data) {
        $("#entity-table > tbody").remove()

        let trHTML = '<tbody>';
        $.each(data, function (i, item) {
            trHTML +=
                '<tr>' +
                '<td>' + item.id + '</td>' +
                '<td>' + item.name + '</td>' +
                '<td>' + item.contents + '</td>' +
                // '<td>' +
                // '<a href="../update/page.html?id=' + item.id + '">Update</a>' +
                // '</td>' +
                // '<td>' +
                // <form action="http://localhost/projects/ExamPHPAngular/backend/delete-entity.php" method="post"
                //               onsubmit="return confirm('Confirm deletion?');">
                //           <input type="hidden" name="id" value="item.id" />
                //           <input type="submit" value="Delete" />
                //         </form>
                // '<a onclick="return confirm(\'Confirm deletion?\');" href="../delete/page.html?id=' + item.id + '">Delete</a>' +
                // '</td>' +
                '</tr>';
        });
        trHTML += '<tbody>'

        entityTable.append(trHTML);
        entityTable.show();
    }
});
