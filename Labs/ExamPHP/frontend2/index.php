<?php
session_start();
if (!isset($_SESSION['name'])) {
    header("Location: http://localhost/projects/ExamPHP/frontend2/login.php");
}
?>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Entity Dashboard</title>
    <!--  <link rel="stylesheet" href="../../../resources/style.css">-->
    <link rel="stylesheet" href="dashboard/style.css">
    <script src="https://code.jquery.com/jquery-latest.min.js"></script>
    <script src="index.js"></script>
</head>

<body>

<a href="login.php">Login</a>
<a href="../backend2/logout.php">Logout</a>

<?php
if (isset($_SESSION['name']))
    echo '<h3>Hi ' . $_SESSION['name'] . '</h3>';
?>

<form action="../backend2/create-entity.php" method="post">
    <label>Give name: </label>
    <input type="text" name="name"><br>

    <label>Give contents: </label>
    <input type="text" name="contents"><br>

    <input id="form-submit" type="submit" value="Create document">
</form>

<div>Documents:</div>
<div id="container">
    <table id="entity-table">
        <thead>
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Contents</th>
        </tr>
        </thead>
    </table>
</div>

<!--<input id="basic-fetch-input" type="submit" value="Fetch all from basic input"><br><br>-->
<!---->
<!--<form id="basic-fetch-form">-->
<!--    <input type="submit" value="Fetch all from basic form">-->
<!--</form>-->
<!---->
<!--<form id="advanced-fetch-form" >-->
<!--    <label for="documents-number">Minimum documents number:</label>-->
<!--    <input id="documents-number" type="text" name="keywords"><br>-->
<!---->
<!--    <input type="submit" value="Fetch from advanced form">-->
<!--</form>-->
<br>
<br>
<div>Current author documents and movies interleaved:</div>
<table id="interleaved-table">
    <thead>
    <tr>
        <th></th>
        <th></th>
        <th></th>
    </tr>
    </thead>
</table>

<br>
<br>

<input id="largest-input" type="submit" value="Get document with largest number of authors">

<table id="documents-table">
    <thead>
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Contents</th>
        <th>Number of authors</th>
    </tr>
    </thead>
</table>

</body>
</html>


