<?php
/**
 * @var $connection
 */
require 'database-connection.php';

$idToFind = $_GET["id"];

$query = "SELECT * FROM websites where id = ?";

$statement = $connection->prepare($query);
$statement->bind_param("i", $idToFind);
$statement->execute();
$statement->bind_result($id, $url);
$statement->fetch();

$entity = new StdClass();
$entity->id = $id;
$entity->url = $url;

echo json_encode($entity);

$statement->close();
$connection->close();
