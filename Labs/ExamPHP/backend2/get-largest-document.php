<?php
/**
 * @var $connection
 */
require 'database-connection.php';

$query = "select
       (select count(*) from authors where documentList like concat('%', d.name, '%')) as count
from documents2 d
order by count desc limit 1;";

$result = mysqli_query($connection, $query);
$row = mysqli_fetch_array($result);
$maxNumber = $row['count'];

$query = "select id, name, contents,
       (select count(*) from authors where documentList like concat('%', d.name, '%')) as count
from documents2 d
having count = '".$maxNumber."'";

$result = mysqli_query($connection, $query);
$documents = array();
while ($row = mysqli_fetch_array($result)) {
    array_push($documents, $row);
}

echo json_encode($documents);

$connection->close();
